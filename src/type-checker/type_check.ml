open Core
open Ast.Ast_types

let rec int_to_type_var_id (i : int) =
    if i <= 25 then
        String.sub "abcdefghijklmnopqrstuvwxyz" ~pos: i ~len: 1
    else
        (int_to_type_var_id (i / 26)) ^
            (String.sub "abcdefghijklmnopqrstuvwxyz" ~pos: (i mod 26) ~len: 1)

let rec join_intersecting_sets l =
    match l with
    | [] | [_] -> l
    | s :: l_tl ->
        let non_intersecting_sets = join_intersecting_sets l_tl in
        let (s, rest) = List.fold non_intersecting_sets ~init: (s, []) ~f: (fun (s1, rest) s2 ->
            if Set.is_empty (Set.inter s1 s2) then
                (s1, s2 :: rest)
            else
                ((Set.union s1 s2), rest)) in
        s :: rest

let group_type_variables tis =
    let open Typed_ast in
    let m = Map.empty (module String) in
    let rec helper = fun tis i m ->
        match tis with
        | [] -> m
        | tis_hd :: tis_tl ->
        match tis_hd with
        | TIVariable(id) ->
            let new_m = (match Map.find m id with
            | None -> Map.set m ~key: id ~data: [i]
            | Some l -> Map.set m ~key: id ~data: (i :: l)) in
            helper tis_tl (i + 1) new_m
        | _ -> helper tis_tl (i + 1) m
    in
    let (_, values) = List.unzip (Map.to_alist (helper tis 0 m)) in
    values

(* Add type error prefix to an error string *)
let prepend_type_error_info_to_string loc error_msg =
    Fmt.str "%s: type error - %s" (loc_to_string loc) error_msg

(* If res is an Ok, does nothing. If it's an Error, prepends type error prefix *)
let prepend_type_error_info loc res =
    match res with
    | Ok _ -> res
    | Error (err) ->
        let error_msg = prepend_type_error_info_to_string loc (Error.to_string_hum err) in
        Error(Error.of_string error_msg)

(*
Combine existing partial application with a new application.
e.g. x[;1] 1 2 3 should end up as x[1 2 3;1]. In this case, we're combining [None; Some(TIInt)]
     (partial_ti_maybes) with [Some(TIArray(TIInt))] (new_ti_maybes) to get the result:
     [Some(TIArray(TIInt)); Some(TIInt)]
*)
let rec combine_arguments partial_ti_maybes new_ti_maybes =
    match partial_ti_maybes with
    | [] -> new_ti_maybes
    | partial_hd :: partial_tl ->
    match new_ti_maybes with
    | [] -> partial_ti_maybes
    | new_hd :: new_tl ->
    match partial_hd with
    | Some _ -> partial_hd :: (combine_arguments partial_tl new_ti_maybes)
    | None -> new_hd :: (combine_arguments partial_tl new_tl)

let get_number_of_operator_params op =
    match op with
    | OpPlus | OpMinus | OpMult | OpDiv | OpMod
    | OpLeq | OpGeq | OpNeq | OpLess | OpGreater | OpEqual
    | OpJoin -> 2
    | OpEach | OpScan | OpOver
    | OpTil -> 1

let create_no_operator_found_error op type_infos =
    let open Typed_ast in
    let error_msg = Fmt.str "no operator %s which takes in arguments (%s)"
        (operator_to_string op)
        (String.concat ~sep: "; " (List.map ~f: type_info_to_string type_infos)) in
    Error (Error.of_string error_msg)

let get_operator_function_type_info op =
    let open Typed_ast in
    match op with
    | OpPlus | OpMinus | OpMult | OpDiv | OpMod
    | OpLeq | OpGeq | OpNeq | OpLess | OpGreater | OpEqual
    | OpJoin ->
        TIFunction([AppOperator(op, [])], [TIVariable("a");TIVariable("b")], TIVariable("c"))
    | OpEach | OpScan | OpOver ->
        TIFunction([AppOperator(op, [])], [TIVariable("a")], TIVariable("b"))
    | OpTil -> TIFunction([AppOperator(op, [])], [TIInt], TIArray(TIInt))

let rec get_identifier_type global_env local_env var_name =
    match get_identifier_type_in local_env var_name with
    | Some te -> Ok(te)
    | None ->
    match get_identifier_type_in global_env var_name with
    | Some te -> Ok(te)
    | None ->
        let error_msg = Fmt.str "no variable named %s" (var_name) in
        Error (Error.of_string error_msg)
and get_identifier_type_in env var_name =
    match env with
    | (var_name_env, type_expr_env) :: env_tail ->
        if var_name = var_name_env then
            Some(type_expr_env)
        else
            get_identifier_type_in env_tail var_name
    | [] -> None

let rec get_product_type_fields type_env type_id =
    match type_env with
    | (type_id_env, type_definition_env) :: type_env_tl ->
        if type_id = type_id_env then
            Ok(type_definition_env)
        else
            get_product_type_fields type_env_tl type_id
    | [] ->
        let error_msg = Fmt.str "no product type named %s" (type_id) in
        Error (Error.of_string error_msg)

let rec get_field_type fields field_name =
    match fields with
    | {name=field_name_env; te=field_te_env} :: fields_tl ->
        if field_name = field_name_env then
            Ok(Typed_ast.upcast_type_expr_to_type_info field_te_env)
        else
            get_field_type fields_tl field_name
    | [] ->
        (*
        TODO: Improve this error message by including the definition of the type including fields
        *)
        let error_msg = Fmt.str "no field named %s" (field_name) in
        Error (Error.of_string error_msg)

(* helper function which extracts the type of a Typed_ast.expr *)
let get_typed_expr_type expr =
    let open Typed_ast in
    match expr with
    | Integer _ -> TIInt
    | Array (_, type_info, _) -> TIArray(type_info)
    | Func (_, type_info, _, _) -> type_info
    | Operator (_, type_info, _) -> type_info
    | Application (_, type_info, _, _) -> type_info
    | FieldAccess (_, type_info, _, _) -> type_info
    | Var (_, type_info, _, _) -> type_info
    | TypeConstructor (_, type_info, _) -> type_info
    | Identifier (_, type_info, _) -> type_info
    | If (_, type_info, _, _, _) -> type_info

 let rec get_array_return_type app index_te_maybes =
    let open Result in
    let open Typed_ast in
    match index_te_maybes with
    | index_te_maybe :: rest ->
        (match app with
        | TIArray(array_te) ->
            get_array_return_type array_te rest >>= fun return_te ->
            (match index_te_maybe with
            | None -> Ok(TIArray(return_te))
            | Some TIInt -> Ok(return_te)
            | Some(TIArray TIInt) -> Ok(TIArray(return_te))
            | _ -> Error (Error.of_string "Can't index array with non-integer type")) (* Indexed with non-int *)
        | _ -> Error (Error.of_string "Can't index a non-array")) (* TODO: Add location info *)
    | [] -> Ok(app)

let rec consolidate_type_info_list tis =
    let open Result in
    let open Typed_ast in
    match tis with
    | [] -> Error (Error.of_string "Can't consolidate empty list")
    | ti :: tis_tl ->
        List.fold_result tis_tl ~init: ti ~f: (fun ti1 ti2 ->
            if ti1 = ti2 then
                Ok(ti1)
            else match (ti1, ti2) with
            | (TIFunction(apps1, arg_tis1, return_ti1), TIFunction(apps2, arg_tis2, return_ti2)) ->
                (*
                TODO [!!!] - Check that arg_tis1 and return_ti1 matches with arg_tis2 and return_ti2
                (not necessarily equal though)
                e.g. (a';b') -> c' matches (a';a') -> b'
                *)
                consolidate_function_types (arg_tis1, return_ti1) (arg_tis2, return_ti2)
                >>= fun (new_arg_tis, new_return_ti) ->
                let [@warning "-3"] new_apps = List.dedup (List.append apps1 apps2) ~compare: (fun x y ->
                    if x = y then 0
                    else 1) in
                Ok(TIFunction(new_apps, new_arg_tis, new_return_ti))
            | _ ->
                let error_msg = Fmt.str "cannot consolidate types %s and %s"
                    (type_info_to_string ti1) (type_info_to_string ti2) in
                Error (Error.of_string error_msg))

(*
Algorithm:
For each unique type variable in each param_tis, create a set of all indices
corresponding to that type variable in that param_tis.
Once that's done, take a list of those sets. For each pair of sets, check for intersection. If they
intersect, union them to form a new set. Repeat the process of joining intersecting sets until none
intersect.
For each set, iterate over each index. Index both param_tis at that index, and consolidate the
types. All types which share a type variable in the return types should consolidate.
*)
and consolidate_function_types (param_tis_1, return_ti_1) (param_tis_2, return_ti_2) =
    let open Typed_ast in
    let open Result in
    let l1 = List.append param_tis_1 [return_ti_1] in
    let l2 = List.append param_tis_2 [return_ti_2] in
    if (List.length l1) <> (List.length l2) then
        let error_msg = "Cannot consolidate functions with different number of parameters" in
        Error (Error.of_string error_msg)
    else
    let groups1 = List.map (group_type_variables l1) ~f: (Set.of_list (module Int)) in
    let groups2 = List.map (group_type_variables l2) ~f: (Set.of_list (module Int)) in
    let joined_sets = join_intersecting_sets (List.append groups1 groups2) in
    (*
    Turn joined sets into list of TIVariables
    Any time there's no type variable associated with an index, there must be 2 non-variable types
    in l1 and l2 at that index. In this case, consolidate them (or error if you can't).
    Once this list of type_infos is creates, loop through it while tracking what you've set
    *)
    let sorted_joined_sets = List.sort joined_sets ~compare: (fun s1 s2 ->
        (* Opening Result overwrites compare *)
        Pervasives.compare (Set.min_elt_exn s1) (Set.min_elt_exn s2)) in
    let get_variable_id_at_index = (fun (i : int) ->
        match List.findi sorted_joined_sets ~f: (fun _ s -> Set.mem s i) with
        | Some (s_i, _) -> Ok(int_to_type_var_id s_i)
        | None ->
            let error_msg = Fmt.str "No set containing index %d" i in
            Error (Error.of_string error_msg)) in
    List.fold_result (List.range 0 (List.length l1)) ~init: [] ~f: (fun tis_rev i ->
        match (List.nth_exn l1 i, List.nth_exn l2 i) with
        | (TIVariable(_), _) | (_, TIVariable(_)) ->
            get_variable_id_at_index i >>= fun id ->
            Ok(TIVariable(id) :: tis_rev)
        | (ti1, ti2) ->
            consolidate_type_info_list [ti1; ti2] >>= fun ti ->
            Ok(ti :: tis_rev))
    >>= fun tis_rev ->
    let tis = List.rev tis_rev in
    let get_non_variable_types_at is =
        List.fold is ~init: [] ~f: (fun tis i ->
            match (List.nth_exn l1 i, List.nth_exn l2 i) with
            | (TIVariable(_), TIVariable(_)) -> tis
            | (TIVariable(_), ti) | (ti, TIVariable(_)) -> ti :: tis
            | (ti1, ti2) -> ti1 :: ti2 :: tis) in
    List.fold_result sorted_joined_sets ~init: (0, tis) ~f: (fun (i,tis) is ->
        let non_var = (get_non_variable_types_at (Set.to_list is)) in
        let (new_i, res) = if 0 = (List.length non_var) then
            (i + 1, Ok(TIVariable(int_to_type_var_id i)))
        else
            (i, consolidate_type_info_list non_var) in
        res >>= fun consolidated_type_info ->
        Ok((new_i, List.mapi tis ~f: (fun i ti ->
            if Set.mem is i then
                consolidated_type_info
            else
                ti))))
    >>= fun (_, final_tis) ->
    let return_ti = List.last_exn final_tis in
    let param_tis = List.slice final_tis 0 ((List.length final_tis) - 1) in
    Ok((param_tis, return_ti))

let rec get_operator_return_type op type_infos =
    let open Result in
    let open Typed_ast in
    let n_params = (get_number_of_operator_params op) in
    if not (n_params = (List.length type_infos)) then
        let error_msg = Fmt.str "operator %s takes %d arguments, but %d arguments were given"
            (operator_to_string op) n_params (List.length type_infos) in
        Error (Error.of_string error_msg)
    else
    match op with
    | OpPlus | OpMinus | OpMult | OpDiv | OpMod
    | OpLeq | OpGeq | OpNeq | OpLess | OpGreater | OpEqual ->
        (match type_infos with
        | [TIInt; TIInt] -> Ok (TIInt)
        | [TIArray(ti1); TIArray(ti2)] | [TIArray(ti1); ti2] | [ti1; TIArray(ti2)] ->
            get_operator_return_type op [ti1; ti2] >>= fun ti -> Ok(TIArray(ti))
        | _ -> create_no_operator_found_error op type_infos)
    | OpJoin ->
        (match type_infos with
        | [TIInt; TIInt] -> Ok (TIArray(TIInt))
        | [TIArray(ti1);TIArray(ti2)] ->
            consolidate_type_info_list [ti1; ti2] >>= fun ti -> Ok(TIArray(ti))
        | [TIArray(ti1);ti2] ->
            consolidate_type_info_list [ti1;ti2] >>= fun ti ->
            Ok(TIArray(ti))
        | _ -> create_no_operator_found_error op type_infos)
    | OpTil ->
        (match type_infos with
        | [TIInt] -> Ok(TIArray(TIInt))
        | _ -> create_no_operator_found_error op type_infos)
    | OpEach ->
        (match type_infos with
        | [TIFunction(apps, param_tis, return_ti)] ->
            let new_param_tis = List.map param_tis ~f: (fun ti -> TIArray(ti)) in
            let new_return_ti = TIArray(return_ti) in
            let app = AppDerivedFunction(op, TIFunction(apps, param_tis, return_ti), []) in
            Ok(TIFunction([app], new_param_tis, new_return_ti))
        | _ -> create_no_operator_found_error op type_infos)
    | OpScan | OpOver ->
        (* TODO: TypeCheck that the first parameter type consolidates with the return type *)
        (match type_infos with
        | [TIFunction(apps, param_tis, return_ti)] ->
            (match param_tis with
            | pti1 :: pti2 :: pti_tl ->
                let new_param_tis = pti1 :: TIArray(pti2) ::
                    (List.map pti_tl ~f: (fun ti -> TIArray(ti))) in
                let new_return_ti = match op with
                    | OpScan -> TIArray(return_ti)
                    | _ -> return_ti in
                let app = AppDerivedFunction(op, TIFunction(apps, param_tis, return_ti), []) in
                Ok(TIFunction([app], new_param_tis, new_return_ti))
            | _ ->
                let error_msg = Fmt.str
                    "operator %s must be called with a function of at least 2 parameters (function of %d parameters provided)"
                    (operator_to_string op) (List.length param_tis) in
                Error (Error.of_string error_msg))
        | _ -> create_no_operator_found_error op type_infos)

let replace_variable_types var_id tis replacement_ti =
    let open Typed_ast in
    List.map tis ~f: (fun ti ->
        match ti with
        | TIVariable(var_id2) ->
            if var_id = var_id2 then replacement_ti
            else ti
        | _ -> ti)

(* TODO: Better function name for this lol *)
let rec function_type_after_a_partial_application arg_tis applied_ti_maybes =
    let open Result in
    let open Typed_ast in
    match applied_ti_maybes with
    | [] -> Ok(arg_tis)
    | applied_ti_maybes_hd :: applied_ti_maybes_tl ->
    match arg_tis with
    (* TODO: better error message *)
    | [] -> Error (Error.of_string "Better message here please (with location info too)")
    | arg_tis_hd :: arg_tis_tl ->
    match applied_ti_maybes_hd with
    | None ->
        function_type_after_a_partial_application arg_tis_tl applied_ti_maybes_tl
        >>= fun res -> Ok(arg_tis_hd :: res)
    | Some applied_ti ->
    if applied_ti = arg_tis_hd then
        function_type_after_a_partial_application arg_tis_tl applied_ti_maybes_tl
    else match arg_tis_hd with
    | TIVariable(id) ->
        let new_arg_tis_tl = replace_variable_types id arg_tis_tl applied_ti in
        function_type_after_a_partial_application new_arg_tis_tl applied_ti_maybes_tl
    | _ ->
        let error_msg =
            "supplied arguments don't match expected function argument types" in
        Error (Error.of_string error_msg)

let unwrap_applied_arg_tis op arg_tis =
    let open Typed_ast in
    let open Result in
    match op with
    | OpEach ->
        List.fold_result arg_tis ~init: [] ~f: (fun tis_rev ti ->
            match ti with
            | TIArray ti -> Ok(ti :: tis_rev)
            | _ ->
                let error_msg = "Cannot unwrap non-array type (OpEach)" in
                Error (Error.of_string error_msg))
        >>= fun unwrapped_tis_rev ->
        Ok(List.rev unwrapped_tis_rev)
    | OpScan | OpOver ->
        (match arg_tis with
        | accum_ti :: tis_tl ->
            List.fold_result tis_tl ~init: [] ~f: (fun tis_rev ti ->
                match ti with
                | TIArray ti -> Ok(ti :: tis_rev)
                | _ ->
                    let error_msg = "Cannot unwrap non-array type (OpScan/OpOver)" in
                    Error (Error.of_string error_msg))
            >>= fun unwrapped_tis_tl_rev ->
            Ok(accum_ti :: (List.rev unwrapped_tis_tl_rev))
        | _ ->
            let error_msg = "I'm too tired to fill this out intelligently TODO" in
            Error (Error.of_string error_msg))
    | _ ->
        let error_msg = Fmt.str "You can't call unwrap_applied_arg_tis with operator %s"
            (operator_to_string op) in
        Error (Error.of_string error_msg)

let rec type_expr (type_env, global_env, local_env) expr =
    let open Result in
    let envs = (type_env, global_env, local_env) in
    match expr with
    | Parsed_ast.Integer (loc, i) -> Ok((local_env, Typed_ast.Integer(loc, i)))
    (* TODO: How do you deal with the type of an empty array *)
    | Parsed_ast.Array (loc, es) ->
        type_expr_list envs es >>= fun (new_local_env, typed_exprs) ->
        (* TODO: If a list of operators, encode possible operators in TIFunction *)
        let types = List.map ~f: get_typed_expr_type typed_exprs in
        (match List.hd types with
            | Some first_type -> Ok(first_type)
            | None -> let error_msg = "empty list with no type" in
            Error (Error.of_string (prepend_type_error_info_to_string loc error_msg)))
        >>= fun first_type ->
        if List.for_all types ~f: (fun type_expr -> type_expr = first_type) then
            Ok((new_local_env, Typed_ast.Array(loc, first_type, typed_exprs)))
        else
            (* TODO: Improve error message to point out different types *)
            let error_msg = "mismatched types within list" in
            Error (Error.of_string (prepend_type_error_info_to_string loc error_msg))
    | Parsed_ast.Func (loc, params, es) ->
        let (i, param_tis_rev) = List.fold params ~init: (0, [])
            ~f: (fun (i, param_tis_rev) {id=_; te_maybe=te_maybe} ->
                match te_maybe with
                | Some te -> (i, (Typed_ast.upcast_type_expr_to_type_info te) :: param_tis_rev)
                | None -> (i + 1, (Typed_ast.TIVariable(int_to_type_var_id i)) :: param_tis_rev)) in
        let param_tis = List.rev param_tis_rev in
        (* TODO: Try to more eagerly evaluate return type *)
        let return_ti = Typed_ast.TIVariable(int_to_type_var_id i) in
        let func_ti = Typed_ast.TIFunction(
            [Typed_ast.AppFunction(params, es, [])],
            param_tis,
            return_ti) in
        Ok((local_env, Typed_ast.Func(loc, func_ti, params, es)))
    | Parsed_ast.Operator (loc, op) ->
        Ok((local_env, Typed_ast.Operator(loc, get_operator_function_type_info op, op)))
    | Parsed_ast.Application (loc, app, expr_maybes) ->
        (* TODO: Try replacing Some/None matches with Option monad (>>=) *)
        List.fold_result expr_maybes ~init: (local_env,[]) ~f: (fun (local_env, te_maybes_rev) expr_maybe ->
            match expr_maybe with
            | Some expr ->
                type_expr (type_env, global_env, local_env) expr >>= fun (new_local_env, typed_expr) ->
                Ok((new_local_env, Some(typed_expr) :: te_maybes_rev))
            | None -> Ok((local_env, None :: te_maybes_rev)))
        >>= fun (new_local_env, te_maybes_rev) ->
        let typed_expr_maybes = List.rev te_maybes_rev in
        type_expr (type_env, global_env, new_local_env) app >>= fun (new_local_env, typed_app) ->
        let applied_ti_maybes = List.map typed_expr_maybes ~f: (fun typed_expr_maybe ->
            match typed_expr_maybe with
            | Some typed_expr -> Some(get_typed_expr_type typed_expr)
            | None -> None) in
        let app_type = get_typed_expr_type typed_app in
        (match app_type with
        | TIArray(array_ti) ->
            get_array_return_type (TIArray(array_ti)) applied_ti_maybes >>= fun return_ti ->
            Ok((new_local_env, Typed_ast.Application(loc, return_ti, typed_app, typed_expr_maybes)))
        | TIFunction (applicatives,expected_arg_tis,app_return_ti) ->
            let res = get_applicatives_return_type
                type_env global_env
                (applicatives,expected_arg_tis,app_return_ti)
                applied_ti_maybes in
            prepend_type_error_info loc res >>= fun return_ti ->
            Ok((new_local_env, Typed_ast.Application(loc, return_ti, typed_app, typed_expr_maybes)))
        | _ ->
            let error_msg = Fmt.str "Cannot apply arguments to expression of type %s"
                (Typed_ast.type_info_to_string app_type) in
            Error (Error.of_string (prepend_type_error_info_to_string loc error_msg)))
    | Parsed_ast.FieldAccess (loc, expr, field_name) ->
        type_expr envs expr >>= fun (new_local_env, typed_expr) ->
        let ti = get_typed_expr_type typed_expr in
        (match ti with
        | TIProduct type_id ->
            prepend_type_error_info loc (get_product_type_fields type_env type_id) >>= fun fields ->
            prepend_type_error_info loc (get_field_type fields field_name) >>= fun field_ti ->
            Ok((new_local_env, Typed_ast.FieldAccess(loc, field_ti, typed_expr, field_name)))
        | _ ->
            let error_msg = Fmt.str "Cannot access field %s, type %s is not a product type"
                field_name (Typed_ast.type_info_to_string ti) in
            Error (Error.of_string (prepend_type_error_info_to_string loc error_msg)))
    | Parsed_ast.Var (loc, type_expr_maybe, var_name, expr) ->
        type_expr envs expr >>= fun (new_local_env, typed_expr) ->
        let type_info = get_typed_expr_type typed_expr in
        let typed_var = Typed_ast.Var (loc, type_info, var_name, typed_expr) in
        (match type_expr_maybe with
        | Some type_expr_annot ->
            let type_info_annot = Typed_ast.upcast_type_expr_to_type_info type_expr_annot in
            if not (type_info_annot = type_info) then
                let error_msg = Fmt.str
                    "type annotation of type %s does not match expression type %s"
                    (Typed_ast.type_info_to_string type_info_annot)
                    (Typed_ast.type_info_to_string type_info) in
                Error (Error.of_string (prepend_type_error_info_to_string loc error_msg))
            else
                Ok(((var_name, type_info) :: new_local_env, typed_var))
        | None -> Ok(((var_name, type_info) :: new_local_env, typed_var)))
    | Parsed_ast.TypeConstructor (loc, type_id) ->
        let open Typed_ast in
        prepend_type_error_info loc (get_product_type_fields type_env type_id) >>= fun fields ->
        let param_tis = List.map fields ~f: (fun {name=_; te=field_te} ->
            Typed_ast.upcast_type_expr_to_type_info field_te) in
        let return_ti = TIProduct (type_id) in
        let type_info = TIFunction([AppTypeConstructor(type_id,[])], param_tis, return_ti) in
        Ok((local_env, Typed_ast.TypeConstructor(loc, type_info, type_id)))
    | Parsed_ast.Identifier (loc, var_name) ->
        prepend_type_error_info loc (get_identifier_type global_env local_env var_name)
        >>= fun type_expr -> Ok((local_env, Typed_ast.Identifier (loc, type_expr, var_name)))
    | Parsed_ast.If (loc, bool_expr, if_expr, else_expr) ->
        type_expr (type_env, global_env, local_env) bool_expr >>= fun (new_local_env, bool_typed_expr) ->
        let bool_type = get_typed_expr_type bool_typed_expr in
        if bool_type <> TIInt then
            let error_msg = Fmt.str "Expression must have Int type, but has %s type instead"
                (Typed_ast.type_info_to_string bool_type) in
            Error (Error.of_string error_msg)
        else
        (* Ignore any env changes in the if and else statements *)
        type_expr (type_env, global_env, new_local_env) if_expr >>= fun (_, if_typed_expr) ->
        type_expr (type_env, global_env, new_local_env) else_expr >>= fun (_, else_typed_expr) ->
        let if_type = get_typed_expr_type if_typed_expr in
        let else_type = get_typed_expr_type else_typed_expr in
        consolidate_type_info_list [if_type;else_type] >>= fun return_ti ->
        let typed_expr = Typed_ast.If(loc, return_ti, bool_typed_expr, if_typed_expr, else_typed_expr) in
        Ok((new_local_env, typed_expr))
(* TODO: Replace List.fold_results with this function *)
and type_expr_list (type_env, global_env, local_env) es =
    let open Result in
    List.fold_result es ~init: (local_env,[]) ~f: (fun (new_local_env, tes_rev) expr ->
        type_expr (type_env, global_env, new_local_env) expr
        >>= fun (new_local_env, typed_expr) ->
        Ok((new_local_env, typed_expr :: tes_rev)))
    >>= fun (new_local_env, tes_rev) ->
    Ok((new_local_env, List.rev tes_rev))

and get_function_return_type type_env global_env params body arg_tis =
    let open Result in
    (* Create local typing environment using arguments *)
    let param_names = List.map params ~f: (fun {id=id;_} -> id) in
    let local_env = List.zip_exn param_names arg_tis in
    type_expr_list (type_env, global_env, local_env) body >>= fun (_, return_tis) ->
    Ok(get_typed_expr_type (List.last_exn return_tis))

(* TODO: This is a gross function, it should be cleaned up *)
and get_applicatives_return_type
        type_env global_env
        (applicatives,expected_arg_tis,app_return_ti)
        applied_ti_maybes : Typed_ast.type_info Or_error.t =
    let open Typed_ast in
    let open Result in
    (* TODO: Check if expected_arg_tis consolidates with applied_ti_maybes (even on partial apply) *)
    List.fold_result applicatives ~init: [] ~f: (fun return_tis applicative ->
        match applicative with
        | AppOperator (_, arg_ti_maybes)
        | AppFunction (_, _, arg_ti_maybes)
        | AppTypeConstructor(_, arg_ti_maybes)
        | AppDerivedFunction(_, _, arg_ti_maybes) ->
        let applied_arg_count = List.count applied_ti_maybes ~f: (fun x ->
            match x with
            | Some _ -> true
            | None -> false) in
        (* If you apply with too many arguments, error *)
        if (List.length applied_ti_maybes) > (List.length expected_arg_tis) then
            let error_msg = Fmt.str
                "Too many arguments applied (%d applied, %d expected)"
                (List.length applied_ti_maybes)
                (List.length expected_arg_tis) in
            Error (Error.of_string error_msg)
        (*
        If number of applied args is less than the expected number of args, create a
        TIFunction with the remaining unapplied arguments as it's args, and with an
        updated applicative
        *)
        else if (List.length expected_arg_tis) > applied_arg_count then
            let new_arg_ti_maybes = combine_arguments arg_ti_maybes applied_ti_maybes in
            function_type_after_a_partial_application expected_arg_tis applied_ti_maybes
            >>= fun new_expected_arg_tis ->
            let open Typed_ast in
            let new_applicative = match applicative with
            | AppOperator (op, _) -> AppOperator(op, new_arg_ti_maybes)
            | AppFunction (params, es, _) -> AppFunction(params, es, new_arg_ti_maybes)
            | AppTypeConstructor (type_id, _) ->
                AppTypeConstructor(type_id, new_arg_ti_maybes)
            | AppDerivedFunction (op, ti, _) ->
                AppDerivedFunction(op, ti, new_arg_ti_maybes) in
            let return_ti = Typed_ast.TIFunction(
                [new_applicative],
                new_expected_arg_tis,
                app_return_ti) in
            Ok(return_ti :: return_tis)
        (*
        If the number of applied args equals the expected number of args, call
        get_operator_return_type to get the return type
        *)
        else if (List.length expected_arg_tis) = applied_arg_count then
            (* Check if applied args match expected arg types *)
            (* List.fold_result applied_ti_maybes ~init: 0 ~f: (fun i ti_maybe ->
                match ti_maybe with
                | None -> Ok(i + 1)
                | Some ti ->
                    consolidate_type_info_list [ti; List.nth_exn expected_arg_tis i]
                    >>= fun _ -> Ok(i + 1)) *)
            let new_arg_ti_maybes = combine_arguments arg_ti_maybes applied_ti_maybes in
            (* Because of the first if block, we know all applied args are Somes *)
            let new_arg_tis = (List.filter_opt new_arg_ti_maybes) in
            (* !!! HACK !!! *)
            (*
            TODO - Clean this up, fix the error message so that it's clear that you applied
            a parameter of the wrong type rather than just saying "failed to consolidate"
            *)
            consolidate_function_types (expected_arg_tis, TIVariable("zzz"))
                (List.filter_opt applied_ti_maybes, TIVariable("zzz")) >>= fun _ ->
            (match applicative with
            | AppOperator (op, _) -> get_operator_return_type op new_arg_tis
            | AppFunction (params, es, _) ->
                get_function_return_type type_env global_env params es new_arg_tis
            | AppTypeConstructor (type_id, _) ->
                (* TODO: Make sure this Errors if the wrong argument types are used *)
                Ok(TIProduct(type_id))
            | AppDerivedFunction (op, func_ti, _) ->
                (match func_ti with
                | TIFunction (applicatives,expected_arg_tis,app_return_ti) ->
                    unwrap_applied_arg_tis op new_arg_tis >>= fun unwrapped_new_arg_tis ->
                    let unwrapped_new_arg_ti_maybes = List.map unwrapped_new_arg_tis
                        ~f: (fun x -> Some x) in
                    get_applicatives_return_type
                        type_env global_env
                        (applicatives,expected_arg_tis,app_return_ti)
                        unwrapped_new_arg_ti_maybes
                    >>= fun return_ti ->
                    (match op with
                    | OpEach | OpScan -> Ok(TIArray(return_ti))
                    | OpOver -> Ok(return_ti)
                    | _ -> Error (Error.of_string "TODO actual error or something unique error message"))
                | _ ->
                    let error_msg = "This shouldn't happen" in
                    Error (Error.of_string error_msg)))
            >>= fun return_ti ->
            Ok(return_ti :: return_tis)
        (*
        Otherwise, error (TODO: What case does this even cover? Does it even cover a case?)
        *)
        else
            let error_msg = "I have no idea what this means" in
            Error (Error.of_string error_msg))
    >>= fun return_tis ->
    (*
    Check that all types match up (not necessarily equal). For TIFunctions, combine their
    applicatives into one set
    *)
    consolidate_type_info_list return_tis

let type_program (Parsed_ast.Program(top_level_es)) =
    let open Result in
    List.fold_result top_level_es ~init: (([],[]), [])
        ~f: (fun ((type_env, global_env), ttles_rev) tle ->
            match tle with
            | Parsed_ast.ProductType (loc, type_id, fields) ->
                let new_type_env = (type_id, fields) :: type_env in
                Ok((new_type_env, global_env),
                    Typed_ast.ProductType(loc, type_id, fields) :: ttles_rev)
            | Parsed_ast.GeneralExpr general_expr ->
                type_expr (type_env, global_env, []) general_expr >>= fun (_, typed_expr) ->
                let new_global_env = match typed_expr with
                | Typed_ast.Var (_, type_info, id, _) -> (id, type_info) :: global_env
                | _ -> global_env in
                Ok(((type_env, new_global_env), Typed_ast.GeneralExpr(typed_expr) :: ttles_rev)))
    >>= fun (_, ttles_rev) ->
        Ok(Typed_ast.Program (List.rev ttles_rev))

let indent_space = "    "

let rec print_expr indent expr =
    let open Typed_ast in
    match expr with
    | Integer (_,i) -> Printf.printf "%sInteger: %d\n" indent i
    | Array (_,t,es) ->
        Printf.printf "%sArray (%s):\n" indent (type_info_to_string t);
        List.iter ~f: (print_expr (indent ^ indent_space)) es
    | Func (_, t, _, _) -> Printf.printf "%sFunc (%s):\n" indent (type_info_to_string t)
    | Operator (_,t,o) ->
        Printf.printf "%sOperator (%s): %s\n" indent (type_info_to_string t) (operator_to_string o)
    | Application (_, t, app, es) ->
        Printf.printf "%sApplication (%s):\n" indent (type_info_to_string t);
        List.iter (Some(app) :: es) ~f: (fun expr_maybe ->
            match expr_maybe with
            | Some e -> print_expr (indent ^ indent_space) e
            | None -> Printf.printf "%s_\n" (indent ^ indent_space))
    | FieldAccess (_, t, e, field_name) ->
        Printf.printf "%sFieldAccess (%s):\n" indent (type_info_to_string t);
        print_expr (indent ^ indent_space) e;
        Printf.printf "%sField Name: %s\n" (indent ^ indent_space) field_name
    | Var (_, t, id, e) ->
        Printf.printf "%sVar %s (%s):\n" indent id (type_info_to_string t);
        print_expr (indent ^ indent_space) e
    | TypeConstructor (_, _, type_id) ->
        Printf.printf "%sTypeConstructor: %s\n" indent type_id
    | Identifier (_, t, id) ->
        Printf.printf "%sIdentifier (%s): %s\n" indent (type_info_to_string t) id
    | If (_, t, bool_expr, if_expr, else_expr) ->
        Printf.printf "%sIf (%s):\n" indent (type_info_to_string t);
        print_expr (indent ^ indent_space) bool_expr;
        Printf.printf "%sThen:\n" indent;
        print_expr (indent ^ indent_space) if_expr;
        Printf.printf "%sElse:\n" indent;
        print_expr (indent ^ indent_space) else_expr

let print_typed_ast (Typed_ast.Program top_level_exprs) =
    let open Typed_ast in
    List.iter top_level_exprs ~f: (fun tle ->
        match tle with
        | ProductType (_, type_id, _) -> Printf.printf "type %s\n" (type_id)
        | GeneralExpr e -> print_expr "" e)
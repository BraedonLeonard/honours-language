open Core

val type_program: Parsed_ast.program -> Typed_ast.program Or_error.t

val print_typed_ast: Typed_ast.program -> unit
open Core
open Ast.Ast_types

(* TODO: Make Array an actual array instead of a list *)
(* TODO: Figure out a more efficient way to handle return types
    (e.g. arrays of actual ints rather than Integers) *)
type interpreter_result =
    | Integer of int
    | Array of interpreter_result list
    | ProductType of string * interpreter_result list
    | Function of parameter list * Typed_ast.expr list * interpreter_result option list
    | Operator of operator * interpreter_result option list
    | TypeConstructor of string * interpreter_result option list
    | DerivedFunction of operator * interpreter_result * interpreter_result option list

(* TODO: Figure out the return type of a call to interpret *)
(* TODO: Make sure interpret can be used in a REPL *)
val interpret_program: Typed_ast.program -> interpreter_result Or_error.t

val interpreter_result_to_string: interpreter_result -> string
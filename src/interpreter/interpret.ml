open Core

(* TODO: Is it possible to avoid having to copy this between .ml and .mli?*)
type interpreter_result =
    | Integer of int
    | Array of interpreter_result list
    | ProductType of string * interpreter_result list
    | Function of Ast.Ast_types.parameter list * Typed_ast.expr list * interpreter_result option list
    | Operator of Ast.Ast_types.operator * interpreter_result option list
    | TypeConstructor of string * interpreter_result option list
    | DerivedFunction of Ast.Ast_types.operator * interpreter_result * interpreter_result option list

let rec interpreter_result_to_string result =
  match result with
  | Integer(i) -> Fmt.str "%d" i
  (* TODO: Make sure this matches the array syntax once the generic one is in *)
  | Array(irs) -> Fmt.str "(%s)" (String.concat ~sep: "; "
    (List.map ~f: interpreter_result_to_string irs))
  | ProductType (type_id, irs) ->
    (* TODO: Include field names when printing *)
    Fmt.str "%s = {%s}" type_id
      (String.concat ~sep: "; " (List.map irs ~f: interpreter_result_to_string))
  | Function (params, _, ir_maybes) ->
    let non_empty = 0 <> List.length ir_maybes in
    Fmt.str "{[%s] ... }%s"
    (String.concat ~sep: "; " (List.map params ~f: Ast.Ast_types.parameter_to_string))
    ((if non_empty then "[" else "") ^
      (String.concat ~sep: "; " (List.map ir_maybes ~f: (fun ir_maybe ->
      match ir_maybe with
      | Some ir -> interpreter_result_to_string ir
      | None -> "")))
      ^ ((if non_empty then "]" else "")))
  | Operator (op, ir_maybes) ->
    let non_empty = 0 <> List.length ir_maybes in
    Fmt.str "%s%s" (Ast.Ast_types.operator_to_string op)
    ((if non_empty then "[" else "") ^
      (String.concat ~sep: "; " (List.map ir_maybes ~f: (fun ir_maybe ->
      match ir_maybe with
      | Some ir -> interpreter_result_to_string ir
      | None -> "")))
      ^ ((if non_empty then "]" else "")))
  | TypeConstructor (type_id, ir_maybes) ->
    let non_empty = 0 <> List.length ir_maybes in
    Fmt.str "%s%s" type_id
      ((if non_empty then "[" else "") ^
        (String.concat ~sep: "; " (List.map ir_maybes ~f: (fun ir_maybe ->
        match ir_maybe with
        | Some ir -> interpreter_result_to_string ir
        | None -> "")))
        ^ ((if non_empty then "]" else "")))
  | DerivedFunction (op, func_ir, ir_maybes) ->
    let non_empty = 0 <> List.length ir_maybes in
    Fmt.str "%s[%s]%s" (Ast.Ast_types.operator_to_string op) (interpreter_result_to_string func_ir)
      ((if non_empty then "[" else "") ^
        (String.concat ~sep: "; " (List.map ir_maybes ~f: (fun ir_maybe ->
        match ir_maybe with
        | Some ir -> interpreter_result_to_string ir
        | None -> "")))
        ^ ((if non_empty then "]" else "")))

(* TODO: Move this into a module to avoid duplicating this between type_check.ml and here *)
let rec combine_arguments partial_maybes new_maybes =
  match partial_maybes with
  | [] -> new_maybes
  | partial_hd :: partial_tl ->
  match new_maybes with
  | [] -> partial_maybes
  | new_hd :: new_tl ->
  match partial_hd with
  | Some _ -> partial_hd :: (combine_arguments partial_tl new_maybes)
  | None -> new_hd :: (combine_arguments partial_tl new_tl)

let rec get_product_type_fields type_env type_id =
  match type_env with
  | (type_id_env, type_definition_env) :: type_env_tl ->
      if type_id = type_id_env then
          Ok(type_definition_env)
      else
          get_product_type_fields type_env_tl type_id
  | [] ->
      let error_msg = Fmt.str "no product type named %s" (type_id) in
      Error (Error.of_string error_msg)


let rec get_number_of_arguments type_env app_ir =
  let open Result in
  match app_ir with
  | Function(params, _, arg_ir_maybes) ->
    let n_applied = List.length (List.filter_opt arg_ir_maybes) in
    Ok((List.length params) - n_applied)
  | Operator(op,arg_ir_maybes) ->
    let n_applied = List.length (List.filter_opt arg_ir_maybes) in
    (match op with
    | OpPlus | OpMinus | OpMult | OpDiv | OpMod
    | OpLeq | OpGeq | OpNeq | OpLess | OpGreater | OpEqual
    | OpJoin ->
      Ok(2 - n_applied)
    | OpEach | OpScan | OpOver
    | OpTil ->
      Ok(1 - n_applied))
  | TypeConstructor(type_id, arg_ir_maybes) ->
    let n_applied = List.length (List.filter_opt arg_ir_maybes) in
    get_product_type_fields type_env type_id >>= fun fields ->
    Ok((List.length fields) - n_applied)
  | DerivedFunction(_, func_ir, arg_ir_maybes) ->
    let n_applied = List.length (List.filter_opt arg_ir_maybes) in
    get_number_of_arguments type_env func_ir >>= fun n_params ->
    Ok(n_params - n_applied)
  | _ ->
    let error_msg = Fmt.str "Cannot get number of arguments for result %s"
      (interpreter_result_to_string app_ir) in
    Error (Error.of_string error_msg)

(* TODO: Move this into a helper module *)
(* let rec print_env env =
  Printf.printf "Environment (length: %d):\n" (List.length env);
  match env with
  | (var_name_env, value) :: env_tail ->
    Printf.printf "%s = %d\n" var_name_env value;
    print_env env_tail
  | [] -> () *)

let rec get_identifier_value global_env local_env var_name =
  match get_identifier_value_in local_env var_name with
  | Some value -> Ok(value)
  | None ->
  match get_identifier_value_in global_env var_name with
  | Some value -> Ok(value)
  | None -> Error (Error.of_string (Fmt.str "No variable named %s" var_name))
and get_identifier_value_in env var_name =
  match env with
  | (var_name_env, value) :: env_tail ->
    if var_name = var_name_env then
      Some(value)
    else
      get_identifier_value_in env_tail var_name
  | [] -> None

let bool_to_int b = if b then 1 else 0

(*
  TODO: It might make more sense to replace the operator with actual callable code (or an
  identifier for callable code) so that you don't need to retain type information to decide
  what code to call
*)
let rec evaluate_operator op args =
  let open Result in
  let open Ast.Ast_types in
  match op with
  | OpPlus | OpMinus | OpMult | OpDiv | OpMod
  | OpLeq | OpGeq | OpNeq | OpLess | OpGreater | OpEqual ->
    (match args with
    | [Integer(x);Integer(y)] -> (
      match op with
      | OpPlus -> Ok(Integer(x + y))
      | OpMinus -> Ok(Integer(x - y))
      | OpMult -> Ok(Integer(x * y))
      | OpDiv -> Ok(Integer(x / y))
      | OpMod -> Ok(Integer(x mod y))
      | OpLeq -> Ok(Integer(bool_to_int (x <= y)))
      | OpGeq -> Ok(Integer(bool_to_int (x >= y)))
      | OpNeq -> Ok(Integer(bool_to_int (x <> y)))
      | OpLess -> Ok(Integer(bool_to_int (x < y)))
      | OpGreater -> Ok(Integer(bool_to_int (x > y)))
      | OpEqual -> Ok(Integer(bool_to_int (x = y)))
      | _ ->
        let error_msg = "I seriously am running out of time to write these" in
        Error (Error.of_string error_msg))
    | [Array(xs);Array(ys)] ->
      (match List.zip xs ys with
      | Some zipped ->
        List.fold_result zipped ~init: [] ~f: (fun zs_rev (x,y) ->
          evaluate_operator op [x;y] >>= fun z ->
          Ok(z :: zs_rev)) >>= fun zs_rev ->
        Ok(Array(List.rev zs_rev))
      | None ->
        let error_msg = Fmt.str "Cannot apply operator %s to lists of unequal lengths (%d and %d)"
          (operator_to_string op) (List.length xs) (List.length ys) in
        Error (Error.of_string error_msg))
    | [x;Array(ys)] ->
      List.fold_result ys ~init: [] ~f: (fun zs_rev y ->
        evaluate_operator op [x;y] >>= fun z ->
        Ok(z :: zs_rev)) >>= fun zs_rev ->
      Ok(Array(List.rev zs_rev))
    | [Array(xs);y] ->
      List.fold_result xs ~init: [] ~f: (fun zs_rev x ->
        evaluate_operator op [x;y] >>= fun z ->
        Ok(z :: zs_rev)) >>= fun zs_rev ->
      Ok(Array(List.rev zs_rev))
    | _ -> Error (Error.of_string
      "(evaluate_operator) The dev forgot to implement part of the interpreter"))
  | OpEach | OpScan | OpOver ->
    (match args with
    | [func_ir] ->
      Ok(DerivedFunction(op, func_ir, []))
    | _ -> Error (Error.of_string
      "(evaluate_operator) The dev forgot to implement part of the interpreter"))
  | OpJoin ->
    (match args with
    | [Integer(i1);Integer(i2)] -> Ok(Array([Integer(i1);Integer(i2)]))
    | [Array(irs1);Array(irs2)] -> Ok(Array(List.append irs1 irs2))
    | [Array(irs);ir] -> Ok(Array(List.append irs [ir]))
    | _ -> Error (Error.of_string
      "(evaluate_operator) The dev forgot to implement part of the interpreter"))
  | OpTil ->
    (match args with
    | [Integer(i)] ->
      if i >= 0 then
        Ok(Array(List.map (List.range 0 i) ~f: (fun i -> Integer(i))))
      else
        Error (Error.of_string "Input to til was negative")
    | _ -> Error (Error.of_string
      "(evaluate_operator) The dev forgot to implement part of the interpreter"))

(*
Naively creates a typed_expr from an expr. Since type checking is already performed, and
the interpreter ignores type info, type info will be a TIVariable for every expr
*)
let rec upcast_expr_to_typed_expr expr =
  let ti = Typed_ast.TIVariable("a") in
  match expr with
  | Parsed_ast.Integer (loc, i) -> Typed_ast.Integer (loc, i)
  | Parsed_ast.Array (loc, es) ->
    let tes = List.map es ~f: upcast_expr_to_typed_expr in
    Typed_ast.Array (loc, ti, tes)
  | Parsed_ast.Func (loc, params, es) -> Typed_ast.Func (loc, ti, params, es)
  | Parsed_ast.Operator (loc, op) -> Typed_ast.Operator (loc, ti, op)
  | Parsed_ast.Application (loc, app, expr_maybes) ->
    let typed_expr_maybes = List.map expr_maybes ~f: (fun em ->
      match em with
      | Some e -> Some(upcast_expr_to_typed_expr e)
      | None -> None) in
    Typed_ast.Application (loc, ti, upcast_expr_to_typed_expr app, typed_expr_maybes)
  | Parsed_ast.FieldAccess (loc, e, field_name) ->
    Typed_ast.FieldAccess(loc, ti, upcast_expr_to_typed_expr e, field_name)
  | Parsed_ast.Var (loc, _, id, e) -> Typed_ast.Var (loc, ti, id, upcast_expr_to_typed_expr e)
  | Parsed_ast.TypeConstructor (loc, id) -> Typed_ast.TypeConstructor(loc, ti, id)
  | Parsed_ast.Identifier (loc, id) -> Typed_ast.Identifier (loc, ti, id)
  | Parsed_ast.If (loc, bool_expr, if_expr, else_expr) ->
    Typed_ast.If(
      loc,
      ti,
      upcast_expr_to_typed_expr bool_expr,
      upcast_expr_to_typed_expr if_expr,
      upcast_expr_to_typed_expr else_expr)

let rec index_array app_ir arg_maybes =
  let open Result in
  match arg_maybes with
  | [] -> Ok(app_ir)
  | arg_maybe :: arg_maybes_tl ->
  match app_ir with
  | Array irs ->
    (match arg_maybe with
    | None ->
      List.fold_result irs ~init: [] ~f: (fun results_rev ir ->
        index_array ir arg_maybes_tl >>= fun result ->
        Ok(result :: results_rev))
      >>= fun results_rev ->
      Ok(Array(List.rev results_rev))
    | Some(Integer i) ->
      (match List.nth irs i with
      | Some result -> index_array result arg_maybes_tl
      | None ->
        let error_msg = Fmt.str "Index out-of-bounds (index %d for list of length %d)"
          i (List.length irs) in
        Error (Error.of_string error_msg))
    | Some(Array index_results) ->
      List.fold_result index_results ~init: [] ~f: (fun results_rev ir ->
        match ir with
        | Integer i ->
          (match List.nth irs i with
          | Some r -> index_array r arg_maybes_tl >>= fun result ->
            Ok(result :: results_rev)
          | None ->
            let error_msg = Fmt.str "Index out-of-bounds (index %d for list of length %d)"
              i (List.length irs) in
            Error (Error.of_string error_msg))
        | _ ->
          let error_msg = "All elements of the index list must be integers" in
          Error (Error.of_string error_msg))
      >>= fun results_rev ->
      Ok(Array(List.rev results_rev))
      | _ ->
        let error_msg = Fmt.str "Cannot index Array with value %s"
          (interpreter_result_to_string app_ir) in
        Error (Error.of_string error_msg))
  | _ ->
    let error_msg = Fmt.str "Cannot index value %s" (interpreter_result_to_string app_ir) in
    Error (Error.of_string error_msg)



let rec interpret_expr (type_env, global_env, local_env) expr =
  let open Result in
  match expr with
  | Typed_ast.Integer (_, i) -> Ok((local_env, Integer(i)))
  | Typed_ast.Array (_, _, es) ->
    interpret_expr_list (type_env, global_env, local_env) es >>= fun (new_local_env, results) ->
    Ok((new_local_env, Array(results)))
  | Typed_ast.Func (_, _, params, es) ->
    let tes = List.map es ~f: upcast_expr_to_typed_expr in
    Ok((local_env, Function(params, tes, [])))
  | Typed_ast.Operator (_, _, op) ->
    Ok((local_env, Operator(op, [])))
  | Typed_ast.Application (_, _, app_expr, arg_expr_maybes) ->
    interpret_expr (type_env, global_env, local_env) app_expr >>= fun (new_local_env, app_result) ->
    List.fold_result arg_expr_maybes ~init: (new_local_env, []) ~f: (fun (local_env, results_rev) expr_maybe ->
      match expr_maybe with
      | Some expr ->
        interpret_expr (type_env, global_env, local_env) expr >>= fun (new_local_env, result) ->
        Ok((new_local_env, Some(result) :: results_rev))
      | None -> Ok((local_env, None :: results_rev)))
    >>= fun (new_local_env, arg_result_maybes_rev) ->
    let arg_result_maybes = List.rev arg_result_maybes_rev in
    apply type_env global_env app_result arg_result_maybes >>= fun result ->
    Ok((new_local_env, result))
  | Typed_ast.FieldAccess (_, _, expr, field_name) ->
    let open Ast.Ast_types in
    interpret_expr (type_env, global_env, local_env) expr >>= fun (new_local_env, result) ->
    (match result with
    | ProductType (type_id, irs) ->
      get_product_type_fields type_env type_id >>= fun fields ->
      (match List.findi fields ~f: (fun _ {name=name;_} -> name = field_name) with
      | Some (i,_) ->
        Ok((new_local_env, List.nth_exn irs i))
      | None ->
        let error_msg = Fmt.str "Cannot access field %s - type %s does not have a field of that name"
          field_name type_id in
        Error (Error.of_string error_msg))
    | _ ->
      let error_msg = Fmt.str "Cannot access field %s - result %s is not a product type"
        field_name (interpreter_result_to_string result) in
      Error (Error.of_string error_msg))
  | Typed_ast.Var (_, _, var_name, expr) ->
    interpret_expr (type_env, global_env, local_env) expr >>= fun (new_local_env, result) ->
    Ok(((var_name, result) :: new_local_env, result))
  | Typed_ast.TypeConstructor (_, _, type_id) -> Ok((local_env, TypeConstructor(type_id, [])))
  | Typed_ast.Identifier (_, _, var_name) ->
    get_identifier_value global_env local_env var_name >>= fun result ->
    Ok((local_env, result))
  | Typed_ast.If (_, _, bool_expr, if_expr, else_expr) ->
    interpret_expr (type_env, global_env, local_env) bool_expr >>= fun (new_local_env, bool_result) ->
    (match bool_result with
    | Integer(i) -> Ok(i)
    | _ -> Error (Error.of_string "This shouldn't happen aaaaaaaaaaaaaaaa"))
    >>= fun i ->
    if i <> 0 then
      interpret_expr (type_env, global_env, new_local_env) if_expr
    else
      interpret_expr (type_env, global_env, new_local_env) else_expr
    >>= fun (_, result) ->
    Ok((local_env, result))
and interpret_expr_list (type_env, global_env, local_env) es =
  let open Result in
  List.fold_result es ~init: (local_env, []) ~f: (fun (local_env, results_rev) expr ->
    interpret_expr (type_env, global_env, local_env) expr >>= fun (new_local_env, result) ->
    Ok((new_local_env, result :: results_rev)))
    >>= fun (new_local_env, results_rev) ->
    Ok((new_local_env, List.rev results_rev))
and evaluate_function type_env global_env params es args =
  let open Result in
  let open Ast.Ast_types in
  let param_names = List.map params ~f: (fun {id=id;_} -> id) in
  let local_env = List.zip_exn param_names args in
  interpret_expr_list (type_env, global_env, local_env) es >>= fun (_, results) ->
  Ok(List.last_exn results)
and apply type_env global_env app_result arg_result_maybes =
  let open Result in
  match app_result with
  | Array (irs) ->
    index_array (Array(irs)) arg_result_maybes
  | Function (params, es, applied_ir_maybes) ->
    let new_applied_ir_maybes = combine_arguments applied_ir_maybes arg_result_maybes in
    if (List.length params) = (List.length (List.filter_opt new_applied_ir_maybes)) then
      evaluate_function type_env global_env params es (List.filter_opt new_applied_ir_maybes)
      >>= fun ir ->
      Ok(ir)
    else
      Ok(Function(params, es, new_applied_ir_maybes))
  | Operator (op, applied_ir_maybes) ->
    let new_applied_ir_maybes = combine_arguments applied_ir_maybes arg_result_maybes in
    get_number_of_arguments type_env (Operator(op, applied_ir_maybes)) >>= fun n_params ->
    if n_params = (List.length (List.filter_opt arg_result_maybes)) then
      evaluate_operator op (List.filter_opt new_applied_ir_maybes) >>= fun ir ->
      Ok(ir)
    else
      Ok(Operator(op, new_applied_ir_maybes))
  | TypeConstructor (type_id, applied_ir_maybes) ->
    let new_applied_ir_maybes = combine_arguments applied_ir_maybes arg_result_maybes in
    get_number_of_arguments type_env (TypeConstructor(type_id, applied_ir_maybes)) >>= fun n_params ->
    if n_params = (List.length (List.filter_opt arg_result_maybes)) then
      let ir = ProductType(type_id, (List.filter_opt new_applied_ir_maybes)) in
      Ok(ir)
    else
      Ok(TypeConstructor(type_id, new_applied_ir_maybes))
  | DerivedFunction (op, func_ir, applied_ir_maybes) ->
    let new_applied_ir_maybes = combine_arguments applied_ir_maybes arg_result_maybes in
    get_number_of_arguments type_env func_ir >>= fun n_params ->
    if n_params = (List.length (List.filter_opt new_applied_ir_maybes)) then
      match op with
      | OpEach ->
        let irs = (List.filter_opt new_applied_ir_maybes) in
        let ir_lists = List.map irs ~f: (fun ir ->
          match ir with
          | Array(ir_list) -> ir_list
          | _ -> []) (* !!! HACK !!!*) in
        let idxs = (List.range 0 (List.length (List.nth_exn ir_lists 0))) in
        List.fold_result idxs ~init: [] ~f: (fun results_rev i ->
          let args = List.map ir_lists ~f: (fun ir_list -> List.nth_exn ir_list i) in
          apply type_env global_env func_ir (List.map args ~f: (fun x -> Some x))
          >>= fun result ->
          Ok(result :: results_rev))
        >>= fun results_rev ->
        Ok(Array(List.rev results_rev))
      | OpScan ->
        let irs = (List.filter_opt new_applied_ir_maybes) in
        let accum_ir = List.hd_exn irs in
        let irs_tl = List.tl_exn irs in
        let ir_lists = List.map irs_tl ~f: (fun ir ->
          match ir with
          | Array(ir_list) -> ir_list
          | _ -> []) (* !!! HACK !!!*) in
        let idxs = (List.range 0 (List.length (List.nth_exn ir_lists 0))) in
        List.fold_result idxs ~init: ([], accum_ir) ~f: (fun (results_rev, accum_ir) i ->
          let args = accum_ir :: (List.map ir_lists ~f: (fun ir_list -> List.nth_exn ir_list i)) in
          apply type_env global_env func_ir (List.map args ~f: (fun x -> Some x))
          >>= fun result ->
          Ok(result :: results_rev, result))
        >>= fun (results_rev,_) ->
        Ok(Array(List.rev results_rev))
      | OpOver ->
        let irs = (List.filter_opt new_applied_ir_maybes) in
        let accum_ir = List.hd_exn irs in
        let irs_tl = List.tl_exn irs in
        let ir_lists = List.map irs_tl ~f: (fun ir ->
          match ir with
          | Array(ir_list) -> ir_list
          | _ -> []) (* !!! HACK !!!*) in
        let idxs = (List.range 0 (List.length (List.nth_exn ir_lists 0))) in
        List.fold_result idxs ~init: accum_ir ~f: (fun accum_ir i ->
          let args = accum_ir :: (List.map ir_lists ~f: (fun ir_list -> List.nth_exn ir_list i)) in
          apply type_env global_env func_ir (List.map args ~f: (fun x -> Some x))
          >>= fun result ->
          Ok(result))
        >>= fun result_ir ->
        Ok(result_ir)
      | _ ->
        let error_msg = Fmt.str "Operator %s cannot be used with DerivedFunction"
          (Ast.Ast_types.operator_to_string op) in
        Error (Error.of_string error_msg)
    else
      Ok(DerivedFunction(op, func_ir, new_applied_ir_maybes))
  | _ ->
    let error_msg = Fmt.str "Cannot apply arguments to result %s"
      (interpreter_result_to_string app_result) in
    Error (Error.of_string error_msg)

(* TODO: Add optional env argument for initializing the environment, and also return the new env *)
let interpret_program (Typed_ast.Program(top_level_es)) =
  let open Result in
  List.fold_result top_level_es ~init: (([],[]), Integer(0))
    ~f: (fun ((type_env, global_env), prev_result) tle ->
      match tle with
      | Typed_ast.ProductType (_, type_id, fields) ->
        let new_type_env = (type_id, fields) :: type_env in
        Ok(((new_type_env, global_env), prev_result))
      | Typed_ast.GeneralExpr expr ->
        interpret_expr (type_env, global_env, []) expr >>= fun (_, result) ->
        let new_global_env = match expr with
        | Typed_ast.Var (_, _, id, _) -> (id, result) :: global_env
        | _ -> global_env in
        Ok(((type_env, new_global_env), result)))
  >>= fun (_, result) -> Ok(result)

(* Interface for the parser *)

open Core

val parse : Lexing.lexbuf -> Parsed_ast.program Or_error.t

val print_parse_tree : Parsed_ast.program -> unit
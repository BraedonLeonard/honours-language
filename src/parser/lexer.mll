{
    open Lexing
    open Parser

    exception SyntaxError of string

    let next_line lexbuf =
        let pos = lexbuf.lex_curr_p in
        lexbuf.lex_curr_p <-
            { pos with
                pos_bol = lexbuf.lex_curr_pos;
                pos_lnum = pos.pos_lnum + 1
            }
}

let digit = ['0'-'9']
let lowercase = ['a'-'z']
let uppercase = ['A'-'Z']
let alpha = lowercase | uppercase
let whitespace = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

let int = '-'?digit+

let id = (lowercase) (alpha | digit | '_')*
let type_id = (uppercase) (alpha | digit | '_')*

rule read_token = parse
    | whitespace { read_token lexbuf }
    | newline { next_line lexbuf; read_token lexbuf }
    | "(" { LPAREN }
    | ")" { RPAREN }
    | "[" { LBRACKET }
    | "]" { RBRACKET }
    | "{" { LCURLY }
    | "}" { RCURLY }
    | ":" { COLON }
    | ";" { SEMICOLON }
    | "<=" { LEQ }
    | ">=" { GEQ }
    | "<>" { NEQ }
    | "<" { LEFT_ANGLE }
    | ">" { RIGHT_ANGLE }
    | "mod" { MOD }
    | "=" { EQUALS }
    | "+" { PLUS }
    | "-" { MINUS }
    | "*" { STAR }
    | "%" { PERCENT }
    | "~" { TILDE }
    | "\\" { BACK_SLASH }
    | "/" { FORWARD_SLASH }
    | "," { COMMA }
    | "til" { TIL }
    | "." { DOT }
    | int { INTEGER (int_of_string (Lexing.lexeme lexbuf)) }
    | "var" { VAR }
    | "if" { IF }
    | "then" { THEN }
    | "else" { ELSE }
    | "Int" { TYPE_INT }
    | "type" { TYPE_TOKEN }
    | id { ID (Lexing.lexeme lexbuf) }
    | type_id { TYPE_ID (Lexing.lexeme lexbuf) }
    | _ { raise ( SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
    | eof { EOF }
open Parsed_ast
open Ast.Ast_types
open Core
open Lexing

(* TODO: Is there someway to rewrite this to use loc_to_string? *)
let get_error_position_string lexbuf =
  let pos = lexbuf.lex_curr_p in
  Fmt.str "Line:%d Position:%d" pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse lexbuf =
  try Ok (Parser.program Lexer.read_token lexbuf) with
  | Parser.Error ->
    let error_msg = Fmt.str "%s: syntax error@." (get_error_position_string lexbuf) in
    Error (Error.of_string error_msg)

let indent_space = "    "

let rec print_expr indent expr =
  match expr with
  | Integer (_,i) -> Printf.printf "%sInteger: %d\n" indent i
  | Array (_, es) -> Printf.printf "%sArray:\n" indent;
    List.iter ~f: (print_expr (indent ^ indent_space)) es
  | Func (_, ps, _) -> Printf.printf "%sFunction with parameters [%s]\n" indent
    (String.concat ~sep: "; " (List.map ~f: parameter_to_string ps))
  | Operator (_, o) -> Printf.printf "%sOperator: %s\n" indent (operator_to_string o)
  | Application (_, app, es) -> Printf.printf "%sApplication:\n" indent;
    List.iter (Some(app) :: es) ~f: (fun expr_maybe ->
      match expr_maybe with
      | Some expr -> print_expr (indent ^ indent_space) expr
      | None -> Printf.printf "%s_\n" (indent ^ indent_space))
  | FieldAccess (_, e, field_name) -> Printf.printf "%sFieldAccess:\n" indent;
    print_expr (indent ^ indent_space) e;
    Printf.printf "%sField Name: %s\n" (indent ^ indent_space) field_name
  | Var (_, maybe_te, id, e) -> let type_str = match maybe_te with
    | Some te -> " (" ^ (type_expr_to_string te) ^ ")"
    | None -> "" in
    Printf.printf "%sVar %s%s:\n" indent id type_str;
    print_expr (indent ^ indent_space) e
  | TypeConstructor (_, type_id) -> Printf.printf "%sTypeConstructor: %s\n" indent type_id
  | Identifier (_, id) -> Printf.printf "%sIdentifier: %s\n" indent id
  | If (_, bool_expr, if_expr, else_expr) ->
    Printf.printf "%sIf:\n" indent;
    print_expr (indent ^ indent_space) bool_expr;
    Printf.printf "%sThen:\n" indent;
    print_expr (indent ^ indent_space) if_expr;
    Printf.printf "%sElse:\n" indent;
    print_expr (indent ^ indent_space) else_expr

let print_parse_tree (Program(top_level_es)) : unit =
  List.iter top_level_es ~f: (fun tle ->
    match tle with
    | GeneralExpr e -> print_expr "" e
    | ProductType _ -> Printf.printf "ProductType print TODO\n")
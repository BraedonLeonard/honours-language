%{
    (* This section is the ocaml header, which can run ocaml code *)
    open Parsed_ast
    open Ast.Ast_types
%}

/*
Token definitions

format:
    %token <type> UID
<type> is an optional type specification which implies that the token contains a value
UID is a capitalized identifier
*/
%token LPAREN
%token RPAREN
%token LBRACKET
%token RBRACKET
%token LCURLY
%token RCURLY
%token COLON
%token SEMICOLON
%token IF THEN ELSE
%token EQUALS LEFT_ANGLE RIGHT_ANGLE GEQ LEQ NEQ
%token PLUS MINUS STAR PERCENT MOD
%token TILDE BACK_SLASH FORWARD_SLASH
%token COMMA TIL
%token DOT
%token <int> INTEGER
%token VAR
%token <string> ID
%token TYPE_INT
%token TYPE_TOKEN
%token <string> TYPE_ID
%token EOF

(* grammar start rule *)
%start program

%type <program> program

%type <type_expr> type_expr
%type <type_expr> type_annot

%type <expr> general_expr

%type <expr option list> argument_list

%type <top_level_expr> product_type_expr

(*
Dummy precedence used to make sure general_expr general_expr general_expr is parsed as
    general_expr (general_expr general_expr)
rather than
    (general_expr general_expr) general_expr
*)
%nonassoc Application

(* List all tokens which can appear at the beginning of an expression *)
%nonassoc LPAREN VAR ID PLUS MINUS STAR LBRACKET LCURLY TYPE_ID DOT TILDE BACK_SLASH FORWARD_SLASH
    EQUALS LEFT_ANGLE RIGHT_ANGLE GEQ LEQ NEQ PERCENT MOD IF ELSE COMMA TIL

%right INTEGER (* Shift INTEGER onto stack *)
(*
The way this works is that a reduce rule's precedence is determined by either the precedence of
the rightmost terminal token, or by the precedence of the token specified by %prec. A shift/reduce
conflict happens when there's a choice between either shifting the lookahead token (i.e. the
next token to be read) or reducing the previous symbols on the stack using a rule. To resolve this,
it first determines whether the reducing rule or the lookahead token has higher precedence. If the
rule has higher precedence, then the reduction is done. If the token has higher precedence, then
the token is shifted onto the stack.
Therefore, if all terminals which can be at the start of an expression have higher priority than
the expression rule has, then shifting is always done instead of reducing.
*)

%%

program:
    | es=separated_list(SEMICOLON, top_level_expr); EOF { Program(es) }

(* TODO: Array/Function type_exprs in the parser/lexer *)
type_expr:
    | TYPE_INT {TEInt}

type_annot:
    | COLON; t=type_expr {t}

top_level_expr:
    | e=product_type_expr { e }
    | e=general_expr { GeneralExpr(e) }

/* $startpos is supplied by Menhir */
general_expr:
    | LPAREN; e=general_expr; RPAREN {e}
    (* 2 or more adjacent integers are parsed as a list *)
    | i=INTEGER { Integer($startpos, i) }
    | is=integer_list_expr { is }
    | f=func { f }
    | o=operator { Operator($startpos, o) }
    | app=general_expr; args=argument_list { Application($startpos, app, args) }
    | app=general_expr; arg=general_expr %prec Application { Application($startpos, app, [Some(arg)]) }
    | access=product_type_field_access { access }
    | VAR; id=ID; t=option(type_annot); EQUALS; e=general_expr %prec Application { Var($startpos, t, id, e) }
    | type_id=TYPE_ID { TypeConstructor($startpos, type_id) }
    | id=ID { Identifier($startpos, id) }
    | e=if_expr { e }

if_expr:
    | IF; bool_expr=general_expr; THEN; if_expr=general_expr; ELSE; else_expr=general_expr
        { If($startpos, bool_expr, if_expr, else_expr) }

product_type_expr:
    | TYPE_TOKEN; name=TYPE_ID; EQUALS; LCURLY;
        fields=separated_nonempty_list(SEMICOLON, product_type_field); RCURLY
        { ProductType($startpos, name, fields) }

product_type_field:
    | name=ID; t=type_annot { {name=name; te=t} }

product_type_field_access:
    | e=general_expr; DOT; field_name=ID { FieldAccess($startpos, e, field_name) }

func:
    | LCURLY; params=parameter_list; body=separated_list(SEMICOLON, general_expr); RCURLY
        { Func($startpos, params, body) }

parameter_list:
    | LBRACKET; params=separated_list(SEMICOLON, parameter); RBRACKET { params }

parameter:
    | id=ID; t=option(type_annot) { {id=id; te_maybe=t} }

(* Need multiple productions to avoid ambiguity of [] becoming either nothing, or 1 None *)
argument_list:
    | LBRACKET; RBRACKET { [] }
    | LBRACKET; arg=general_expr; RBRACKET { [Some(arg)] }
    | LBRACKET; arg1=option(general_expr); SEMICOLON; args=separated_nonempty_list(SEMICOLON, option(general_expr));
        RBRACKET { arg1 :: args }

integer_list_expr:
    (* TODO: Is it possible to get the correct $startpos for each integer? *)
    | i=INTEGER; is=nonempty_list(INTEGER) {
        Array($startpos, Integer($startpos, i) :: (List.map (fun i -> Integer($startpos, i)) is)) }

(*
%inline means that 'operator's will be replaced by the specific operator they are
e.g. Instead of:
    o=operator; ...
You would get:
    o=MINUS;...
    o=PLUS;...
    etc.
*)
%inline operator:
    | PLUS { OpPlus }
    | MINUS { OpMinus }
    | STAR { OpMult }
    | PERCENT { OpDiv }
    | LEQ { OpLeq }
    | GEQ { OpGeq }
    | NEQ { OpNeq }
    | LEFT_ANGLE { OpLess }
    | RIGHT_ANGLE { OpGreater }
    | MOD { OpMod }
    | EQUALS { OpEqual }
    | TILDE { OpEach }
    | BACK_SLASH { OpScan }
    | FORWARD_SLASH { OpOver }
    | COMMA { OpJoin }
    | TIL { OpTil }
# Prerequisites
Tested on Linux Mint 19 with:
- OCaml 4.05.0
- Dune 2.8.2
- opam 2.0.4

# To Run
3 executables can be built and run: `parse_test.exe`, `type_check_test.exe`, and `interpret_test.exe`.

`parse_test.exe` parses the text file provided as a command line argument, and prints the parse tree.

`type_check_test.exe` parses and type checks the text file provided as a command line argument, and prints both the parse tree and the typed parse tree.

`interpret_test.exe` parses, type checks, and interprets the text file provided as a command line argument, and prints the parse tree, the typed parse tree, and the final result of the program.

Example of interpreting a file:
```
dune exec ./src/interpret_test.exe ./test/array-join-test.bl
```